const csvtojson = require("csvtojson");

function convertCSVToJSON(filePath){
    return csvtojson()
        .fromFile(filePath)
        .then((data) => {
            return data;
        });
}

module.exports = convertCSVToJSON;